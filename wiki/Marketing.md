# Marketing
## Corporate Design

### Name FoodNotify
Der Name der Firma lautet **FoodNotify GmbH**. Bitte auf die Schreibweise achten mit dem großen **F** & **N**.  

_Richtig:_  
**F**ood**N**otify  

_Falsch:_  
~~foodnotify~~  
~~Foodnotify~~  
~~foodNotify~~  
~~FOODNOTIFY~~

### Farben
Grün `#78b62c`  
Orange `#ec7c06`  
Graublau `#3d4352`  
Gelb `#ffcc00`

### Schrift
Die FoodNotify Systemschrift ist "Roboto". Die Schrift kann frei von Google Fonts heruntergeladen werden und im System zur Benützung installiert werden.  
[Roboto auf Google Fonts](https://fonts.google.com/specimen/Roboto)

### Logos


#### Logo (normal)
Das reguläre FoodNotify Logo für alle Anwendungsbereiche   
Transparenter Hintergrund (png)  
Hintergrund weiß (jpg)
Logo als Vektor (svg, eps)

#### Logo outline (mit weißer Umrandung)
Wird benutzt für dunkle Hintergründe mit transparentem Hintergrund

![Logo outline](https://www.foodnotify.com/wp-content/uploads/Homepage/logo.png)


#### Carrie


#### Karotte


#### Modul Icons




## Content
### Zitate & Referenzen
### Textbaustein

 
##Signatur
Hier findest du die FoodNotify Signatur als Vorlage zum Download und als Code. Diese kann einem Code-Editor (bspw. Brackets, Sublime Text, Webstorm, TextEdit, o.ä.) bearbeitet werden.

### Signatur Vorlage Download
[Signatur Vorlage](https://www.foodnotify.com/signature_vorlage_download)

### Signatur Vorlage Code
 
 ```html
 <style type="text/css">
	a { text-decoration:none;}
	/* unvisited link */
a:link {
    color: #747474;
}
/* visited link */
a:visited {
    color: #747474;
}
/* mouse over link */
a:hover {
    color: #747474;
}
/* selected link */
a:active {
    color: #747474;
}</style>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<table width="520" style="font-family: 'Arial, Helvetica', sans-serif;">
  <tbody>
    <tr>
      <td><img src="https://www.foodnotify.com/wp-content/uploads/foodnotify_logo_130px.jpg" align="center" width="130" nosend="1"></td>
      <td style="border-left:solid 1px #EE7C00; padding-left:10px;">
      	<table width="350">
          <tbody>
            <tr>
              <td height="35" width="250" style="font-size: 14px; color:#EE7C00;">Mag. Thomas Primus <br/> <span style="font-size:12px;color:#0f0f0f; padding-bottom:10px;">CEO & Co-Founder</span></td>
            </tr>
            <tr>
              <td height="30" style="font-size:12px; color:#9a9a9a; line-height: 18px;"><a href=https://www.google.at/maps/place/Zirkusgasse+13,+1020+Wien/@48.2154911,16.3811825,17z/data=!3m1!4b1!4m2!3m1!1s0x476d0709da4b52b1:0x9073ca2a531a8c53=>Zirkusgasse 13/3, 1020 Wien, AT</a></td>
            </tr>
            <tr>
              <td height="30" style="font-size:12px; color:#9a9a9a;"> <font color="#EE7C00">m:</font> <a href="tel:+436502404222"> +43 650 2404 222 </a><font color="#EE7C00"> t:</font> <a href="tel:+4312672404"> +43 1 267 2404 </a> <br/> <font color="#EE7C00">e:</font> <a href=mailto:thomas@foodnotify.com> thomas@foodnotify.com </a> <font color="#EE7C00"> w:</font> <a href=https://www.foodnotify.com> foodnotify.com </a> <br /><br /></td>
            </tr>
            <tr>
              <td height="35">
 				<a href="https://www.facebook.com/FoodNotify"><img src="https://www.foodnotify.com/wp-content/uploads/facebook_g25px.gif" width="25" nosend="1" style="border-radius:5px;"></a>
                <a href="https://twitter.com/foodnotify"><img src="https://www.foodnotify.com/wp-content/uploads/twitter_g25px.gif" width="25" nosend="1" style="border-radius:5px;"></a>
                <a href="https://plus.google.com/b/102876072991309370964/+Foodnotify/posts"><img src="https://www.foodnotify.com/wp-content/uploads/googleplus_g25px.gif" width="25" nosend="1" style="border-radius:5px;"></a>
                <a href="https://www.youtube.com/channel/UCgSr3sde9X_5TBBe4ALFYnw"><img src="https://www.foodnotify.com/wp-content/uploads/youtube_g25px.gif" width="25" nosend="1" style="border-radius:5px;"></a>              
				<a href="https://www.instagram.com/foodnotify"><img src="https://www.foodnotify.com/wp-content/uploads/instagram_g25px.gif" width="25" nosend="1" style="border-radius:5px;"></a>
              </td>
            </tr>
          </tbody>
        </table>
        <tr>
        	<td colspan="3" style="font-size:8px;color:#747474;"><br />FoodNotify ist eine Marke der <strong>CookWanted GmbH I FN: 415299i I UID: ATU68675828</strong>.</br>Die Informationen in diesem E-mail sind ausschließlich für den Adressaten bestimmt. Zugriff auf diese Nachricht durch Dritte ist nicht erlaubt. Wenn Sie nicht der richtige Adressat sind, ist jede Form der Veröffentlichung, Vervielfältigung oder Verbreitung sowie jegliche Handlung, die aufgrund dieser Nachricht unternommen oder unterlassen wird, verboten und könnte gegen das Gesetz verstoßen. Bitte benachrichtigen Sie sofort den Absender.
        	</td>
        </tr>
        <tr>
        	<td colspan="3" style="font-size:8px;color:#747474;">The information contained in this e-mail is intended solely for the addressee. Access to this e-mail by anyone else is unauthorized. If you are not the intended recipient, any form of disclosure, reproduction, distribution or any action taken or refrained from in reliance on it, is prohibited and may be unlawful. Please notify the sender immediately.
        	</td>
        </tr>
      </td>
    </tr>
  </tbody>
</table>
 ```
