# Kooperationen
## Bestehende Kooperationen
### Datenanbindungen & Handel
Hier finden sich alles Kooperationen von Handelspartnern und Partnern von Datenanbindungen:


#### AGM
www.agm.at 
 
- auf FoodNotify mit Shop verbunden  
- Kooperation seit: 2016

**Ansprechpartner**

- Markus Mathis (Koordination Standorte) m.mathis@agm.at  
- Sabine Gabl (Marketing) s.gabl@rewe-group.at 

**Marketingaktivitäten**

- Flugblatt (alle 2 Wochen neu) 
- Produktspezifische Kataloge (Fleisch, Obst und Gemüse, Wein, Non-Food, Wild)
- Diverse „Mini-Rezeptbücher“
- Pressemeldungen
- Sehr nachhaltig und regional  Nachhaltigkeitswochen
- LMIV Folder wird auf Website angeboten
- Onlineshop
- Unter Services eigener Button für Foodnotify
- Gründungsmitglied von United Against Waste
- Newsletter

---
#### BÄKO
www.baeko.at

- Kooperation seit: 2017
- kein Vertrag abgeschlossen

**Ansprechpartner**

- Manuel Feichtlbauer manuel.feichtlbauer@baeko.at


**Marketingaktivitäten**

- Magazin (BÄKO Backszene)
- Newsletter
- Nachhaltigkeitsbericht 2015
- Wirtschaftskompass
- Technikkatalog
- Folder
- Werbemittel: Sammelpässe, Plakatdruckservice und Bildschirmwerbung
- Flyer

---
####Bindi
www.bindi.at

- Kooperation seit
- kein Vertrag abgeschlossen

**Ansprechpartner**

- Niels Konzack (Geschäftsführender Gesellschafter) niels.konzack@bindi.de
- Daniela Obermeyer (Assistentin der Geschäftsleitung und Marketingleitung) obermeyer@bindi.de
- Domenico Bonanno (Vertriebsleitung & Key Account Management) bonanno@bindi.de (noch nicht in Hub Spot)

**Marketingaktivitäten**

- Newsletter
- Facebook
- Instagram
- YouTube Kanal
- Besuchen Messen wie GAST, Internorga oder Intergastra
- Magazin Magazzino
- Wir sind nicht auf Kooperationsseite (sie bei uns schon) 
- Werbemittel: Dessert Karte, Pyramidenaufsteller, Tischaufsteller, Tischaufsteller Prisma, Poster, Eis Karte, Eis Tafel, Gehwegaufsteller, Eis-Fahne, Eis-Pappbecher, individuelle Eis- und Dessertkarteneinleger
- Gastronomie-Katalog
- Infos zu LMIV auf Website und extra Tabelle zum Download
- Pressemeldungen

---
#### Eurogast Kiennast
www.eurogast.kiennast.at

- Kooperation seit

**Ansprechpartner**

- Alexander Kiennast alexander.kiennast@kiennast.at 

**Marketingaktivitäten**

- Onlineshop
- Facebook
- YouTube Kanal
- Eurogast Journal
- Eurogast Magalog Genuss 360 – Fachmagazin
- Verschiedene Folder
- Verschiedene Flyer
- Eurogast-Katalog
- Eurodiskont-Katalog
- Pressemeldungen

---
#### iSi
www.isi.com

- Kooperation seit

**Ansprechpartner**

- Christian Steuber (Global Culinary Director) christian.steuber@isi.com

**Marketingaktivitäten**

- Facebook
- YouTube Kanal
- Twitter
- Instagram
- Newsletter
- News-Seite (wo auch ein Artikel über FoodNotify steht)  wie Blog
- Besuchen und veranstalten verschiedene Events
- Besuchen Messen wie GAST
- Kooperations-Seite (FoodNotify erscheint)

---
#### Kröswang
www.kröswang.at

- Kooperation seit
- auf FoodNotify mit Shop verbunden

**Ansprechpartner**

- Kröswang Manfred (Geschäftsführer) Manfred.kroeswang@kroeswang.at 
- Holl Thomas (Leiter Marketing, PR) thomas.holl@kroeswang.at


**Marketingaktivitäten**

- Facebook
- Frische Katalog - Magazin
- Produktkatalog
- Newsletter
- Onlineshop
- Kooperationsseite (FoodNotify wird nicht erwähnt)
- Besuchen Messen wie GAST und Intergastra

---
#### Metro Cash & Carry Österreich
www.metro.at

- Kooperation seit 2015
- Shop auf FoodNotify

**Ansprechpartner**

- Alexa Kazda (Corporate Communications) alexa.kazda@metro.at 

**Marketingaktivitäten**

- Google Plus
- Facebook
- YouTube Kanal
- Verschiedene Flugblätter
- Newsletter
- Pressemeldungen
- Werbemittel: Bedruckte Papierservietten, Besteck Gravur, Individuelle Textilien, Arbeitskleidung einzigartig bestickt, Büroartikel Gravur, Porzellan Beschriftung, Individuelle Pizzakartons, Gläserbeschriftung& Veredelung
- Infos zu LMIV
- Infos und Tipps zur Registrierkassenpflicht (mit empfohlenen Systemen)
- Nachhaltigkeit und Regionalität spielt eine Rolle
- Treueaktion ProfiCool
- Sortiments/Gastronomie-Kataloge
- gourMETRO Kunden-Magazin
- Infos, Tipps und Checklisten für Veranstaltungen (vor allem für Vereine)
- Tipps und Empfehlungen für Privatzimmervermietungen
- Tipps und Tricks zur Unternehmensgründung  Unternehmer Starterpaket
- Kostenlos eigene Website einrichten
- E-Rechnungen
- Unter „weitere Services für Gastronomen“ wird auf FoodNotify verwiesen


---
#### Metro Cash & Carry Deutschland 
www.metro.de

- Kooperation seit
- auf FoodNotify mit Shop verbunden

**Ansprechpartner**

- Bettina Tolksdorf (Allergene Management) bettina.tolksdorf@metro-cc.de

**Marketingaktivitäten**

- 

---
#### Wedl
www.wedl.com

- Kooperation seit
- auf FoodNotify mit Shop verbunden

**Ansprechpartner**

- Birgit Delmarco (Leitung Marketing) birgit.delmarco@wedl.com 
- Lorenz Wedl (Junior Chef) lorenz.wedl@wedl.com 
- Katharina Graus (Design) design@wedl.com 


**Marketingaktivitäten**

- 

---
#### Wiberg
www.wiberg.eu

- auf FoodNotify eigene Rezeptdatenbank

**Ansprechpartner**

- Iris Walcher (Head of Agency | Text, Concept, Web) iris.walcher@frutarom.eu

**Marketingaktivitäten**

- 

---
#### TopCC (Schweiz)
www.topcc.ch

- gehört zu Spar
- wir auf FoodNotify mit Shop verbunden
- finaler Vertrag fertig besprochen aber noch nicht unterzeichnet

**Ansprechpartner**

- Dominic Möckli dominic.moekli@topcc.ch

**Marketingaktivitäten**

- 

---
#### Transgourmet
www.transgourmet.at

- Infos

**Ansprechpartner**

- AT: Bettina Fleiss (Bereichsleitung Marketing) bettina.fleiss@transgourmet.at 
- AT: Karin Meisl (Nationale Verkaufsleitung) karin.meisl@transgourmet.at 
- DE: Erik Lydzius erik.lydzius@transgourmet.de 
- CH: Sabrina Solca sabrina.solca@transgourmet.ch 


**Marketingaktivitäten**

- 

---
#### Wiesbauer
www.wiesbauer-gourmet.at

- auf FoodNotify mit Shop verbunden

**Ansprechpartner**

- Ernst Stocker stocker@wiebauer-gourmet.at 

**Marketingaktivitäten**

- 

---
#### Herba cuisine
www.herbacuisine.de

- unser Investor ohne Vertrag in Zukunft Kooperation


**Ansprechpartner**

- Jens Puhl (Geschäftsführer) j.puhl@herbacuisine.de 

**Marketingaktivitäten**

- 

---
#### Sonnentor
www.sonnentor.at

- Rezeptdatenbank bei FoodNotify integriert

**Ansprechpartner**

- Michael Scheibenpflug (Teamleitung Gastronomie) michael.scheibenpflug@sonnentor.at
- Sonja Aigner (Leitung Marketing & New Media) sonja.aigner@sonnentor.at 
- Verena Eder (Marken-Botschafter | Marketing & Werbemittel) verena.eder@sonnentor.at
- Markus Haffert (Online Projekt Manager) markus.haffert@sonnentor.at 


**Marketingaktivitäten**

- B2B auch bisschen --> Hotels z.B. bei Tee Ecke --> Schulung von Key Accounter also auch dranbleiben

---
#### Name
www..at

- Infos

**Ansprechpartner**

- 

**Marketingaktivitäten**

- 

---


###Schulen

#### GAFA Gastgewerbefachschule Judenplatz
www.gafa.ac.at

**Ansprechpartner**

- David Grandegger (Fachvorstand) dgrandeg@gafa.ac.at 
- Werner Sedlacek (Direktor) w.sedlacek@gafa.ac.at

---

#### Hotelfachschule Heidelberg
www.hotelfachschule-heidelberg.de

**Ansprechpartner**

- Melanie Hilse hilse@hotelfachschule-heidelberg.de
- Uwe Kälberer kaelberer@online.de

---

### Branchenlösungen

#### CHECK de Cuisine
www.checkdecuisine.de

**Ansprechpartner**

- Daniel Schwanitz (Geschäftsführer) ds@maxit4u.de

---

#### E2N Gastro

www.gastro.e2n.de 

**Ansprechpartner**

- Simon Mohr (Geschäftsführer) mohr@e2n.de 

---
#### Wesual
www.wesolutions.at

**Ansprechpartner**

- Denis Matic (Projektmanagement & Entwicklung) denis@wesolutions.at


### Kassensysteme

#### Gastrofix
www.gastrofix.com

**Ansprechpartner**

- Loukas Louka (Country Manager Austria) l.louka@gastrofix.com 

---
#### orderbird
www.orderbird.com

**Ansprechpartner**

- Patrick patrick@orderbird.com 
Hans Keil hans@orderbird.com 
 
---
#### ready2order
www.ready2order.com

**Ansprechpartner**

- Christoph Schachner (Content Marketing) christoph.schachner@ready2order.com 

---
#### TIPOS
www.tipos.at

**Ansprechpartner**

- Richard Angerer (Geschäftsführung & Vertrieb) r.angerer@tipos.at

---
####Gastro-MIS
www.amadeus360.de

- Amadeus 360 (Kassensystem von Gasto-MIS)  
- Kooperation seit 20.02.2018

**Ansprechpartner**

- Mirco Till (Geschäftsführer) mirco.till@gastro-mis.de
- Heike Weisser (Leitung Vertrieb & Support) heike.weisser@gastro-mis.de

---

### Sonstige Kooperationen

## Potenzielle Kooperationen
### Datenanbindungen
### Schulen
### Branchenlösungen
### Kassensysteme
### Sonstige Kooperationen
