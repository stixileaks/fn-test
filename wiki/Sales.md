# Sales
## Händler
### Österreich
- AGM (Adeg Großmarkt – REWE Konzern)
- Biogast (gehört zu Kastner, spezialisiert auf BIO-Produkte)
- Eurogast (Zusammenschluss mehrerer Händler)
 - Kiennast
 - Pilz & Kiennast-
 - Interex
 - Landmarkt
 - Kärntner Legro
 - Zuegg
 - Speckbacher
 - Grissemann
 - Riedhart
 - Sinnesberger
 - Almauer)
- HOGAST - Einkaufsgenossenschaft
- Intergast
- Kastner (Will nicht mit uns arbeiten – Angst vor Neuem)
- Kröswang
- Metro
- R&S Gourmet Express (wurde gekauf von Kröswang)
- Rungis Express (gekauft von Metro)
- Transgourmet (ehemals Pfeiffer)
- Wedl
- Wiesbauer

### Deutschland
- Chefs Culinar
- DEHOGA – Einkaufsgenossenschaft und Verband
- Edeka
- Handelshof
- HOGAST - Einkaufsgenossenschaft
- Intergast
- Kröswang
- Metro
- Rungis Express (gekauft von Metro)
- Servicebund (Zusammenschluss mehrerer inhabergeführter Unternehmen)
- Transgourmet

### Schweiz
- Top CC (Spar)
- Transgourmet
- Pistor
- Prodega/Growa

##Mitbewerb
Konkurrent | Website
--- | --- |
necta |www.necta-group.com/
Kost | www.kost.at/
Delegate |www.delegate-group.com/
Orderlion | www.orderlion.at
Calcmenu | www.eg-software.com/
Selly.biz | www.selly.biz
Orga-Soft | www.orga-soft.de
Alacalc | www.alacalc.de
PPM | http://www.ppm-system.de/
Gastrosmart | www.gastro-smart.com
Bon Appetit | www.solutionsforchefs.com
MBS5 | www.mbs5.de
Nuts | www.nutritional-software.at
Jomosoft | http://cc-softwareundconsulting.de/

### FoodMeUp
Frankreich  

Bieten LMIV Kennzeichnung, Inhaltsstoffkatalog, Datenblätter, Produktionsplanung, Spezifische Entwicklungen, API, Verbindung mit internen Systemen
System nur auf Französisch verfügbar. Programm bietet viel weniger Features. Billigste Version 49€/Monat.

### Ideolys
Frankreich  

Easilys Restauration   
Bieten Rezeptmanagement, Ernährungspläne, DGE-Konformität, sofort bewertete Menüs, LMIV-Aufdrucke, Produktionsmanagement, Soll-Lagerbestände, Inventare, Kaufanalysen, Finanzplaner, Bestellungen, Rechnungen und Guthaben nachverfolgen
Nur auf Englisch und Französisch verfügbar. Bieten zusätzlich noch ein Equipment und Waste Management Tool an. 


# Personas
## P1
Einzelbetrieb  
zB.: el Hans

## P2
Mehrere Betriebe, inhabergeführt  
zB.: Café Landtmann (Familie Querfeld)

## P3
Mehrere Betriebe, konzerngeführt  
zB.: Austria Trend Hotels

## P4
Big Player, ab 100 Standorte  
zB.: Enchilada, LSG

## P5
Großhändler, Gastro Consulter, Schnittstellenpartner  
zB.: WKO, bioAustria, AMA, Genussregionen, ready2order, AGM, etc.

## P6
Datenmanagement  
zB.: Metro

## P7
P7 - Schulen, EDU Version  
zB.: Gastgewerbefachschule

## X1
fake E-Mail-Adressen od. Personen wo wir nichts rausfinden können
