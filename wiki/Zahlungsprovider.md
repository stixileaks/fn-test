#Zahlungsabwicklung
##Hobex

Abwickler SEPA und Kreditkartenzahlungen in FoodNotify  
**Hotline:** [+43 662 2255 0](+4366222550)  
**Website:** www.hobex.at

### Problemlösung
####Zahlung von Zahlungsstelle abgewiesen
Bei Kunden bei denen ohne Grund die Zahlung von der Zahlungsstelle abgewiesen wird, bitte die folgenden Nummer nutzen um Kunden zu entsperren.  
Deutsche Banken schicken "Sperrlisten" an Zahlungsprovider wenn Folgekarten ausgestellt werden um die alten ungültig zu machen. Manchmal werden diese Sperren nicht mehr aufgehoben, wodurch es zu Problemen mit der Zahlung kommen kann. Bei Hobex gibt es zwei Stellen, die für solche Fragen zuständig sind. Die nachfolgenden Nummern können hier helfen.

- [+43 662 2255 413](tel:+436622255413)
- [+43 662 2255 410](tel:+436622255410)

---

##Wirecard


##Secure Trading

