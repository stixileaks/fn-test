#Praktische Software
## Apple

Name | Beschreibung | Website
---|---|---
Alfred | Workflows und Automatisierung | https://www.alfredapp.com/
Brackets | Code Editor (z.B.: HMTL, CSS, JavaScript, etc.) | http://brackets.io/
Enpass | Passwort Manager | https://www.enpass.io/
Filezilla | FTP-Client | https://filezilla-project.org/
Handbrake | Video Transcoder & Converter | https://handbrake.fr/
Hazel | Automatisierung für Ordner und Dokumente | https://www.noodlesoft.com/
ImageOptim | Bildkomprimierer für Web-Images | https://imageoptim.com/mac
Macdown | Markdown Editor | https://macdown.uranusjr.com/
Spectacle | Fenstermanagement für Arbeitsbereiche | https://www.spectacleapp.com/

## Windows

Name | Beschreibung | Website
---|---|---
Enpass | Passwort Manager | https://www.enpass.io/
Filezilla | FTP-Client | https://filezilla-project.org/
