# Personal

## Team Directory
Im Zeiterfassungstool E2N findet sich auch eine komplette Liste aller Teammitglieder inkl. Kontaktdaten (Name, Telefeonnummer, Email und Abteilung).  
[Link zur Kontaktliste](https://perso.e2n.de/secure/kontakte.jsf)

## Zeiterfassung
Die Zeiterfassung erfolgt über das Tool E2N ([e2n.de](https://perso.e2n.de)) und kann per App oder WebApp erledigt werden.  
### Pausen
Pausen werden automatisch im System erfasst und müssen nicht "gestempelt" werdnen.
### Urlaub
Urlaubsanträge können direkt in E2N erfasst werden. Dort findet auch die Freigabe, sowie die Übersicht des Resturlaubes statt.

## Reisekosten & Diäten
Im nachfolgenden Abschnitt findest du alle Infos zur ERfassung und Abrechnung von Reisekosten, Diäten, Nächtigungsgelder und km-Geld.
### Diäten

### Spesen

### km-Geld
