#Wichtige Informationen

## Links
### User
Informationen | Links
--- | ---
UID | [VIES] (http://ec.europa.eu/taxation_customs/vies/)
GLN | [WKO Firmen A-Z](https://firmen.wko.at/Web/SearchSimple.aspx)
 | [GEPIR GS1](http://www.gepiraustria.org/)
IBAN & BIC | [IBAN Rechner](https://www.iban-rechner.de/)


#Informationen FoodNotify

### Anschrift
#### Österreich
FoodNotify GmbH  
Zirkusgasse 13/3  
1020 Wien

Firmenbuchnummer: 415299i  
Firmenbuchgericht: Handelsgericht Wien  
Behörde gem. ECG: Magistrat Wien  
UID-Nr.: ATU 68675828  
Unternehmensgegenstand: Dienstleistungen in der automatischen Datenverarbeitung und Informationstechnik  
Kammer: WKO Wien, Sparte: BS Information und Consulting, FV UBIT

#### Deutschland
FoodNotify  
c/o Hey Group GmbH  
Rosenthaler Straße 13  
10119 Berlin  
Deutschland  
Email: berlin(@)foodnotify.com  
Telefon: +49 30 22 385 328

#### Schweiz
FoodNotify  
c/o European Invest AG  
Neuhofstrasse 84  
6345 Neuheim  
Schweiz  
mail: schweiz(@)foodnotify.com  
Telefon: 0800 24 04 525

##Bankverbindung
Erste Bank AG  
IBAN: AT392011182513442700  
BIC: GIBAATWWXXX
