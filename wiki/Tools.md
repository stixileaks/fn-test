##Tools
### Hubspot
ist ein Marketing-, Sales- und CRM-Tool das im Browser genutzt wird. Unsere Homepage ist direkt mit Hubspot verbunden und sobald sich ein User registriert hat, werden Verhalten und Interessen automatisch aufgezeichnet. Viele Inhalte unserer Homepage laufen über Hubspot, wie zum Beispiel das Meeting-Tool, diverse Landing Pages oder unser Newsletter. 
Hubspot ist in Marketing und Sales geteilt. Im Sales Bereich werden Kundenkontakte hinzugefügt, bearbeitet und aktuell gehalten. Im Bereich Kontakte können Kunden gesucht, gefiltert und bearbeitet werden. Jeder Kunde verfügt über ein eigenes Kundenprofil in dem neben Kontaktdaten auch E-Mail- und Telefon-Verkehr, Meeting Notizen und Userverhalten aufgezeichnet werden. Dies erleichtert uns, den Überblick über unsere Kontakte zu bewahren. Kontakte können auch Sales Mitarbeitern zugewiesen werden, um klar darzustellen wer der aktuelle Ansprechpartner ist.

Jeder potentielle Kunde wird in die sogenannte Deal Pipeline aufgenommen (im Bereich Deals) und bekommt eine Lifecycle-Stage zugewiesen. Kunden die momentan unsere Testversion nutzen, werden automatisch als Lead / Trial angelegt und können nach erfolgreichem Abschluss zu Closed Won oder andernfalls zu Closed Lost verschoben werden.
Im Bereich Unternehmen können Unternehmen gesucht, gefiltert und bearbeitet werden.
Unter Sales-Tools sind mehrere nützliche Tools vorhanden: 

Unter Vorlagen können E-Mail Vorlagen erstellt und bearbeitet werden. Unter Messages gelangt man zur Übersicht unseres Support-Chats, der auf unserer Homepage angeboten wird. (Um für unsere Kunden als online zu erscheinen, MUSS diese Seite geöffnet bleiben.) Unter Meetings können Kunden, die unsere Help-Seite besuchen, einen individuellen Beratungstermin buchen. Dafür muss der Sales Mitarbeiter seine Verfügbarkeiten im Vorhinein angeben. 
Übern den Button links oben wechselt man in den Marketing Bereich. Unter Kontakte können, genau wie im Sales Bereich, Kontakte und Unternehmen gesucht, gefiltert und bearbeitet werden. 

Unter Workflows können automatisierte Abläufe erstellt werden und bearbeitet werden. 

Ein wichtiges Tool ist im Bereich Listen zu finden. Hier können normale oder smarte Listen erstellt werden, die sich laufend nach ausgewählten Kriterien selbst aktualisieren. Eine bereits voreingestellte Scoring-Liste ist am linken Seitenrand im Ordner Scoring zu finden. Je nach Verhalten des Users werden vom System automatisch Punkte an den Kontakt verteilt. In der Scoring-Liste können Kontakte nach ihrem aktuellen Score gereiht und somit als qualitativer Lead identifiziert werden. 
Unter Content werden hauptsächlich unsere Landing Pages, Blogartikel und Newsletter erstellt, verwaltet und analysiert. Der FoodNotify-Newsletter wird nicht monatlich, sondern anlassbezogen versendet. 
Unter Social Media werden alle Plattformen gleichzeitig verwaltet. Unter Monitoring findet man eine aktuelle Übersicht über Posts und Mentions. Im Bereich Publishing hat man die Möglichkeit Beiträge direkt auf allen Kanälen (außer Instagram) zu posten oder vorzuplanen. Auf unseren Social Media Plattformen positionieren wir uns als „Gastro-Ratgeber“ und versuchen unsere Follower über Neues von FoodNotify, sowie aktuelle Themen in der Gastronomie zu informieren. (Auf Social Media sind wir generell mit allen Followern per „du“)
> _Kleiner Tipp zum Schluss:_ Der beste Weg sich mit Hubspot anzufreunden ist sich überall einmal durchzuklicken und sich aktiv mit dem Programm auseinanderzusetzen :smile:

### Asana
Asana ist unser Projektmanagementtool, welches uns hilft den Überblick zu bewahren und keine Deadlines zu verpassen. Das Programm ist über den Browser abrufbar und wird als individuelle und kollektive To-Do-Liste genutzt. 

Unter My Tasks kannst du deine persönliche To-Do-Liste abrufen und verwalten. Zusätzlich werden alle Aufgaben die dir in anderen Projekten zugewiesen wurden angezeigt. Für jeden Task können Due Dates und Assignees vergeben werden, um klar festzuhalten wer wofür verantwortlich ist und wann die jeweilige Deadline ist. Im Bereich Inbox findest du Neuigkeiten zu deinen Tasks. Größere Projekt werden auf der linken Seite unter Projects aufgelistet. Bei jedem Task gibt es die Option zu kommentieren, Follower hinzuzufügen, oder Dateien hochzuladen. Unter Team Calendar gelangt man zu einer Übersicht aller eingetragener Deadlines.

### Slack
### Office365 (Outlook, Kalendar, OneDrive etc.)
